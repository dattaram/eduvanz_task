import React from 'react'

export default function Header(props) {
    return (
        <div className='headerMain flex'>
            <p className="pageName">{props.title}</p>
            <div className="headerSecondPart">
                {props.children}
            </div>
        </div>
    )
}
