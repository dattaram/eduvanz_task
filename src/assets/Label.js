import React from 'react'

export default function Label(props) {
    return <p className='label'>{props.label} {props.required && <span className='required'>*</span>}</p>
}
