import React from 'react'

export default function Loader(props) {
    return (
        <div className="pageLoaderMain" {...props}> 
            <span className="pageLoader"></span>
        </div>
    )
}
