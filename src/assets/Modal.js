import React, { useEffect, useRef } from 'react'
import useClickoutSide from '../customHooks.js/useClickoutSide'

export default function Modal(props) {
    const ref=useRef()

    useClickoutSide(ref,()=>{
        props.outSideClick()
    });
    return (
        <>
            {props.open &&
                <div className='modalBackground'>
                    <div className="flexCenter formBox" >
                        <div className=" formBoxMain"ref={ref}>
                            <p className="formTitle">{props.title}</p>
                            {props.children}
                        </div>
                    </div>
                </div>
            }
        </>
    )
}
