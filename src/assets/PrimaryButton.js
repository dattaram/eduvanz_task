import React from 'react'

export default function PrimaryButton(props) {
return (
        <button className={`primaryButton ${props.customclass}`} {...props}>
            {props.loader ? <span className="buttonLoader"></span>:''} {props.button}
        </button>
    )
}
