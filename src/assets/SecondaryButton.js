import React from 'react'

export default function SecondaryButton(props) {
    return <button className='secondaryButton' {...props} >{props.button}</button>
}
