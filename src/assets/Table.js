import React from 'react'

export default function Table(props) {
    return (
        <table className="tableMain">
            <thead>
                <tr className="tableRow">
                    {props.columns.map(data =>(
                        <th className='tableHeader'>{data.name}</th>
                    ))}
                </tr>
            </thead>
            <tbody>
                {props.children}
            </tbody>
        </table>
    )
}
