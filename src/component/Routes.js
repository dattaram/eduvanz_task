import React,{lazy, Suspense} from 'react'
import {BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import SideNav from './navbar/SideNav'
import Loader from '../assets/Loader'

const LazyUser=lazy(()=>import('./user/Users'))
const LazyUserReport=lazy(()=>import('./user/UserReport'))
const LazyUserView=lazy(()=>import('./user/UserView'))

export default function Routes() {
    return (
        <Router>
            <div className="navPage">
                <SideNav />
                <Suspense fallback={<Loader></Loader>}>
                    <Switch>
                        <Route path='/' exact component={LazyUser} />
                        <Route path='/userReport' exact component={LazyUserReport} />
                        <Route path='/user/:Id' exact component={LazyUserView} />
                    </Switch>
                </Suspense>
            </div>
        </Router>
    )
}
