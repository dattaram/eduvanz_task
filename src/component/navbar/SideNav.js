import React, { useState, useEffect, useRef } from 'react'
import {Link, NavLink} from 'react-router-dom'
import useClickOutSide from '../../customHooks.js/useClickoutSide'

export default function SideNav() {
    const[menu,setMenu]=useState(false)
    const handleClick=()=>{
        setMenu(true)
    }

    const ref=useRef()
    
    useClickOutSide(ref,()=>{
        if(menu){setMenu(false)}
    });
    return (
        <>
            <div className={`${menu && 'mobileMenu'}`}>
                <div className={`sideNav ${menu && 'sideNavOpen'}`} ref={ref}>
                    <Link to='/' className="navTag logoMain">
                        <img src="/logo192.png" alt='logo' className='logo'/>
                    </Link>
                    <div className="navTagMain">
                        <NavLink to="/" exact className="navTag" activeClassName="activeNavTag">User Details</NavLink>
                        <NavLink to="/userReport" exact className="navTag" activeClassName="activeNavTag">User Report</NavLink>
                    </div>
                </div>
            </div>
            <div className="mobileNav flexSpaceBetween">
                <Link to='/' className="logoMain">
                    <img src="/logo192.png" alt='logo' className='logo'/>
                </Link>
                <img src='/menu.png' alt='menu' className="menuIcon" onClick={handleClick} />
            </div>
        </>
    )
}
