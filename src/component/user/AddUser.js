import React, { useState, useEffect } from 'react'
import Label from '../../assets/Label'
import Input from '../../assets/Input'
import SecondaryButton from '../../assets/SecondaryButton'
import PrimaryButton from '../../assets/PrimaryButton'
import cogoToast from 'cogo-toast'
import { BASEURL, USER } from '../api/APIEndpoints'
import APIServices from '../api/APIServices'

export default function AddUser(props) {
    const [state,setState]=useState({
        name:'',
        profession:'',
        locality:'',
        address:'',
        age:'',
        dob:''
    })
    const [isSubmitting,setSubmitting]=useState(false)

    const handleChange=(e)=>{
        setState({...state,[e.target.name]:e.target.value})
    }
    const ageData=
        state.dob !=="" ? Math.floor((new Date().getTime()-new Date(state.dob).getTime())/(1000*60*60*24*365.25)) +' '+ 'Years' : ""

    const handleSubmit=(e)=>{
        e.preventDefault()
        setSubmitting(true)
        if(state.name==="" || state.locality==="" || state.address==="" || state.dob==="" || state.profession==="" || state.guest===""){
            cogoToast.error("All Fields are Required")
            setSubmitting(false)
        }else{
            setTimeout(async()=>{
                const data={
                    id:props.id,
                    name:state.name,
                    age:ageData,
                    dob:state.dob,
                    address:state.address,
                    profession:state.profession,
                    locality:state.locality,
                    guest:state.guest
                }
                const url=BASEURL+USER
                const req=await new APIServices().post(url,data)
                if(req.error){
                    setSubmitting(false)
                    cogoToast.error('Something wents wrongs')
                }else{
                    props.closeForm()
                    props.data(data)
                }
            },1000)
        }
    }

    const cancelForm=()=>{
        props.closeForm()
    }

    console.log(state.name==="" || state.locality==="" || state.address==="")

    return (
        <form className="mainForm" >
            <div className="flexSpaceBetween wrap">
                <div className="formTag">
                    <Label required label="Name" />
                    <Input 
                        type='text' 
                        name='name' 
                        value={state.name} 
                        onChange={handleChange} 
                    />
                </div>

                <div className='formTag'>
                    <Label required label="Date of Birth" />
                    <Input 
                        type='date' 
                        name='dob' 
                        value={state.dob} 
                        onChange={handleChange} 
                    />
                </div>

                <div className='formTag'>
                    <Label label="Age" />
                    <Input 
                        type='text' 
                        value={ageData}
                        readOnly
                    />
                </div>

                <div className='formTag'>
                    <Label required label="Profession" />
                    <select
                        name="profession"
                        value={state.profession}
                        onChange={handleChange}
                        className="formInput"
                    >
                        <option></option>
                        <option value="student">Student</option>
                        <option value="employed">Employed</option>
                    </select>
                </div>

                <div className='formTag'>
                    <Label required label="Locality" />
                    <Input 
                        type="text"
                        name="locality"
                        value={state.locality}
                        onChange={handleChange}
                    />
                </div>

                <div className='formTag'>
                    <Label required label="Guest" />
                    <select
                        name="guest"
                        value={state.guest}
                        onChange={handleChange}
                        className="formInput"
                    >
                        <option></option>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                </div>

                <div className='formTag'>
                    <Label required label="Address" />
                    <textarea 
                        rows="3"
                        cols="20"
                        maxLength="50"
                        name="address"
                        value={state.address}
                        onChange={handleChange}
                        className="formInput"
                    />
                </div>
            </div>
            <div className="flexEnd">
                <SecondaryButton 
                    button='Cancel' 
                    type='button' 
                    style={{marginRight:'10px'}} 
                    onClick={cancelForm}
                />
                <PrimaryButton
                    button='Submit' 
                    type="button" 
                    onClick={handleSubmit} 
                    disabled={isSubmitting}
                    customclass={isSubmitting ? 'loaderButtonStyle':''}
                    loader={isSubmitting ? true:false}
                />
            </div>
        </form>
    )
}
