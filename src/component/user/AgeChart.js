import React from 'react'
import { Bar } from 'react-chartjs-2'

export default function AgeChart(props) {
    
    const ageData=props.age.map(data=>parseInt(data))
    const firstAge=ageData.filter(data=>data>=13 && data<=18).length
    const secondAge=ageData.filter(data=>data>=19 && data<=25).length
    const thirdAge=ageData.filter(data=>data>25).length
    const ageChartData={
        labels:["13-18","19-25","25+"],
        datasets:[
            {
                label:'Age Range',
                backgroundColor:['rgba(75,192,192,1)','rgba(75,192,12,1)','rgba(244,96,96,1)'],
                borderColor:'rgba(11,11,11,1)',
                borderWidth:1,
                data:[firstAge,secondAge,thirdAge]
            }
        ]
    }
    return (
        <div className="flexCenter chartBox">
            <Bar
                data={ageChartData}
                options={{
                    maintainAspectRatio:false,
                    title:{
                        display:true,
                        text:'Age Range User Data',
                        fontSize:20
                    },
                    scales:{
                        yAxes:[{
                            ticks:{
                                beginAtZero:true,
                                fontSize:15,
                                fontColor:'#000'
                            }
                        }]
                    }
                }}
            />
        </div>
    )
}