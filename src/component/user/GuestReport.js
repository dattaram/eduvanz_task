import React from 'react'
import { Bar } from 'react-chartjs-2'

export default function GuestReport(props) {
    const guest=props.guest.reduce((a,b)=>a+b,0)

    const guestChartData={
        labels:["Guests"],
        datasets:[
            {
                label:'Total Guest',
                backgroundColor:'rgba(75,192,192,1)',
                borderColor:'rgba(11,11,11,1)',
                borderWidth:1,
                data:[guest]
            }
        ]
    }
    return (
        <div className="flexCenter chartBox">
            <Bar
                data={guestChartData}
                options={{
                    maintainAspectRatio:false,
                    title:{
                        display:true,
                        text:'Total Guest User Data',
                        fontSize:20
                    },
                    scales:{
                        yAxes:[{
                            ticks:{
                                beginAtZero:true,
                                fontSize:15,
                                fontColor:'#000'
                            }
                        }]
                    }
                }}
            />
        </div>
    )
}
