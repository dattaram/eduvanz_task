import React from 'react'
import { Bar } from 'react-chartjs-2'

export default function LocalityChart(props) {
    var locality=props.locality
    var countData=[]
    var uniqueLocality=[]
    for(var value of locality){
        if(uniqueLocality.indexOf(value)=== -1){
            uniqueLocality.push(value)
        }
    }
    var sortedArr=[]
    var count=0
    sortedArr=locality.sort()
    for (var i = 0; i < sortedArr.length; i = i + count) {
        count = 1;
        for (var j = i + 1; j < sortedArr.length; j++) {
          if (sortedArr[i] === sortedArr[j])
            count++;
        }
        countData.push(count)
      }
    const localityChartData={
        labels:uniqueLocality.sort(),
        datasets:[
            {
                label:'Locality Range',
                backgroundColor:'red',
                borderColor:'rgba(11,11,11,1)',
                borderWidth:1,
                data:countData
            }
        ]
    }
    return (
        <div className="flexCenter chartBox">
            <Bar
                data={localityChartData}
                options={{
                    maintainAspectRatio:false,
                    title:{
                        display:true,
                        text:'Locality Range User Data',
                        fontSize:20
                    },
                    scales:{
                        yAxes:[{
                            ticks:{
                                beginAtZero:true,
                                fontSize:15,
                                fontColor:'#000'
                            }
                        }]
                    }
                }}
            />
        </div>
    )
}
