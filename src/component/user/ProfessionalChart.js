import React from 'react'
import { Bar } from 'react-chartjs-2'

export default function ProfessionalChart(props) {
    const profession=props.profession
    var countData=[]
    var sortedArr=[]
    var count=0
    sortedArr=profession.sort()
    for (var i = 0; i < sortedArr.length; i = i + count) {
        count = 1;
        for (var j = i + 1; j < sortedArr.length; j++) {
          if (sortedArr[i] === sortedArr[j])
            count++;
        }
        countData.push(count)
      }
    const professionalChartData={
        labels:["Employed","Student"],
        datasets:[
            {
                label:'Professional Range',
                backgroundColor:['rgba(75,192,192,1)','rgba(75,192,12,1)','rgba(244,96,96,1)'],
                borderColor:'rgba(11,11,11,1)',
                borderWidth:1,
                data:countData
            }
        ]
    }
    return (
        <div className="flexCenter chartBox">
             <Bar
                data={professionalChartData}
                options={{
                    maintainAspectRatio:false,
                    title:{
                        display:true,
                        text:'Professional Range User Data',
                        fontSize:20
                    },
                    scales:{
                        yAxes:[{
                            ticks:{
                                beginAtZero:true,
                                fontSize:15,
                                fontColor:'#000'
                            }
                        }]
                    }
                }}
            />
        </div>
    )
}
