import React, { useEffect, useState } from 'react'
import {Bar} from 'react-chartjs-2'
import { BASEURL, USER } from '../api/APIEndpoints'
import APIServices from '../api/APIServices'
import AgeChart from './AgeChart'
import LocalityChart from './LocalityChart'
import ProfessionalChart from './ProfessionalChart'
import Header from '../../assets/Header'
import GuestReport from './GuestReport'
import Loader from '../../assets/Loader'

export default function UserReport() {
    const [user,setUser]=useState([])
    const [error,setError]=useState(false)
    
    const fetchUser=async()=>{
        const url=BASEURL+USER
        const res=await new APIServices().get(url)
        if(res.status=='200' || !res.status){
            const userData=res.results.data
            setUser(userData)
            setError(false)
        }else{
            setError(true)
        }
    }

    useEffect(()=>{
        fetchUser()
    },[])
    
    return (
        <div className="mainPage">
            <Header title="User Report"></Header>
            <div className="page">
                <div className="pageBackground">
                    {user.length>0 ?
                        <div className='flexAround wrap'>
                            <AgeChart age={user.map(data=>data.age)} />
                            <LocalityChart locality={user.map(data=>data.locality)} />
                            <ProfessionalChart profession={user.map(data=>data.profession)}/>
                            <GuestReport guest={user.map(data=>data.guest)} />
                        </div>:
                        !error ? <Loader style={{width:'100%',height:'100%',background:'transparent'}} /> :<p className="noData">Too Many Request</p>
                    }
                </div>
            </div>
        </div>
    )
}
