import React, { useState } from 'react'
import Table from '../../assets/Table'
import SecondaryButton from '../../assets/SecondaryButton'
import {Link} from 'react-router-dom'

export default function UserTable(props) {
    const [columns]=useState([
        {id:1,name:'Name'},
        {id:2,name:'Age'},
        {id:3,name:'Date of Birth'},
        {id:4,name:'Profession'},
        {id:5,name:'Guest'},
        {id:6,name:'Locality'},
        {id:7,name:'Address'},
        {id:8,name:"Action"}
    ])

    
    return (
        <>
            <div className="desktopView">
                <Table columns={columns}>
                    {props.user.length>0 ?
                        props.user.map(data=>(
                            <tr className="tableRow" key={data.id}>
                                <td className="tableData">{data.name}</td>
                                <td className="tableData">{data.age}</td>
                                <td className="tableData">{data.dob}</td>
                                <td className="tableData">{data.profession}</td>
                                <td className="tableData">{data.guest}</td>
                                <td className="tableData">{data.locality}</td>
                                <td className="tableData">{data.address}</td>
                                <td className="tableData">
                                    <Link to={`/user/${data.id}`}>
                                        <SecondaryButton button="View"/>
                                    </Link>
                                </td>
                            </tr>
                        )):
                        <div className="noDataBox filterNoData">
                            <p className="noData">No Data Found</p>
                        </div>
                    }            
                </Table>
            </div>

            <div className="mobileView">
                {props.user.length>0 ?
                    props.user.map(data=>(
                        <div className="cardMain">
                            <div className="cardBox">
                                <div className="userInfo">
                                    <p className='userName'>{data.name}</p>
                                    <p className="userAge">
                                        <span>DOB:</span> {data.dob}({data.age}) 
                                    </p>
                                </div>
                                <div className="flex marginBottom">
                                    <p className="cardHead">Profession:</p>
                                    <p className="cardData">{data.profession}</p>
                                </div>
                                <div className="flex marginBottom">
                                    <p className="cardHead">Guest:</p>
                                    <p className="cardData">{data.guest}</p>
                                </div>
                                <div className="flex marginBottom">
                                    <p className="cardHead">Address:</p>
                                    <p className="cardData">{data.address}</p>
                                </div>
                                <div className="flex marginBottom">
                                    <p className="cardHead">Locality:</p>
                                    <p className="cardData">{data.locality}</p>
                                </div>
                            </div>
                            <Link to={`/user/${data.id}`} className='cardViewButton'>
                                <SecondaryButton button="view" />
                            </Link>
                        </div>
                    )):
                    <div className="noDataBox filterNoData">
                        <p className="noData">No Data Found</p>
                    </div>
                }
            </div>
        </>
    )
}
