import React, { useState, useEffect } from 'react'
import { BASEURL, USER } from '../api/APIEndpoints'
import APIServices from '../api/APIServices';
import Header from '../../assets/Header';
import Loader from '../../assets/Loader'

export default function UserView(props) {
    const [userData,setUserData]=useState({})
    const [error,setError]=useState(false)
    const userView=async()=>{
        const { match: { params } } = props;
        console.log(params)
        // const url = BASEURL+USER+`/${params.Id}`
        const url = BASEURL+USER+`/1`
        const res=await new APIServices().get(url)
        if(!res.error || res.status=='200'){
            const data=res.results
            setUserData(data)
            setError(false)
        }else{
            setError(true)
        }
    }

    useEffect(()=>{
        userView()
    },[])

    const clickBack=()=>{
        window.history.go(-1);
        return false;
    }
    return (
        <div className='mainPage'>
            <Header title="User Detail"></Header>

            <div className="page">
                <div className="pageBackground">
                    {Object.keys(userData).length>0 ?
                        <>
                            <div className="backClick" onClick={clickBack}>
                                <p> Back </p>
                            </div>
                            <div className="flexCenter userDataSpacing">
                                <p className="">Name:</p>
                                <p>{userData.name}</p>
                            </div>
                            <div className="flexCenter userDataSpacing">
                                <p className="">Age:</p>
                                <p>{userData.age}</p>
                            </div>
                            <div className="flexCenter userDataSpacing">
                                <p className="">Date of Birth:</p>
                                <p>{userData.dob}</p>
                            </div>
                            <div className="flexCenter userDataSpacing">
                                <p className="">Profession:</p>
                                <p>{userData.profession}</p>
                            </div>
                            <div className="flexCenter userDataSpacing">
                                <p className="">Locality:</p>
                                <p>{userData.locality}</p>
                            </div>
                            <div className="flexCenter userDataSpacing">
                                <p className="">Guest:</p>
                                <p>{userData.guest}</p>
                            </div>
                            <div className="flexCenter userDataSpacing">
                                <p className="">Address:</p>
                                <p>{userData.address}</p>
                            </div>
                        </>:
                        !error ? <Loader style={{width:'100%',height:'100%',background:'transparent'}} /> :<p className="noData">Too Many Request</p>
                    }               
                </div>
            </div>
        </div>
    )
}
