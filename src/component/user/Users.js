import React, { useState, useEffect } from 'react'
import Header from '../../assets/Header'
import Input from '../../assets/Input'
import PrimaryButton from '../../assets/PrimaryButton'
import Modal from '../../assets/Modal'
import AddUser from './AddUser'
import UserTable from './UserTable'
import { BASEURL, USER } from '../api/APIEndpoints'
import APIServices from '../api/APIServices'


export default function Users() {
    const [open,setOpen]=useState(false)

    const handleClick=()=>{
        setOpen(!open)
    }
    const [user,setUser]=useState([])
    // const user=[
    //     {id:2,name:'datta',locality:'mumbai',age:'10',dob:'20/10/2010',guest:0,profession:'student'}
    // ]
    const [filterUser,setFilterUser]=useState([])
    const [search,setSearch]=useState("")

    const fetchUser=async()=>{
        const url=BASEURL+USER
        const res=await new APIServices().get(url)
        console.log(res)
        if(!res.error || res.status=='200'){
            const userData=res.results.data
            setUser(userData)
        }
    }

    const handleChange=(e)=>{
        const filter = user.filter(data =>(
            data.locality.toLowerCase().includes(e.target.value.toLowerCase()) ||
            data.name.toLowerCase().includes(e.target.value.toLowerCase())
        ))
        setSearch(e.target.value)
        setFilterUser(filter)
    }

    useEffect(()=>{
        fetchUser()
    },[])
    console.log(user,filterUser)
    return (
        <div className='mainPage'>
            <Header title='Users Details'>
                <div className="flexSpaceBetween">
                    <div className='searchBox'>
                        <Input 
                            name="search" 
                            onChange={handleChange}
                            placeholder="Search users by locality or name"
                        />
                    </div>
                    <PrimaryButton button="Add User" onClick={handleClick} />
                </div>
            </Header>

            <div className="page">
                <div className="pageBackground">
                    {user.length>0 ?
                        <UserTable
                            user={search===""?user:filterUser}
                        />:
                        <div className="noDataBox">
                            <p className="noData">No Data Found</p>
                        </div>
                    }
                </div>
            </div>
            <Modal
                open={open}
                outSideClick={handleClick}
                title="Add User"
            >
                <AddUser 
                    id={user.length+1}
                    closeForm={()=>setOpen(false)}
                    data={(data)=>setUser([...user,data])}
                />
            </Modal>
        </div>
    )
}
